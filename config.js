"use strict";

const minimist = require("minimist");

const config = minimist(process.argv.slice(2), {
  string: ["channel"],
  default: {
    maxqueue: 10,
    discord_delaymin: 5000,
    discord_delaymax: 120000,
    discord_backoff: 2,
    chatsock_retry: 200,
  },
});
for (let k of "sockpath token channel".split(" "))
  if (!config[k]) throw `--${k}=... required`;

module.exports = config;
