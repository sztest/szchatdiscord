"use strict";

const EventEmitter = require("events");
const net = require("net");
const readline = require("readline");
const config = require("./config");

const mod = new EventEmitter();
module.exports = mod;

const rxesc = (s) => s.replace(/[.*+?^${}()|[\]\\]/g, "\\$&");
const shutdown = new RegExp("^" + rxesc("*** Server shutting down"), "i");

let mainSock;

const recv = (sock) => (msg) => {
  if (sock !== mainSock)
    return mod.emit("log", {
      warn: "sock !== mainSock",
      msg,
    });

  if (!shutdown.test(msg)) mod.emit("msg", msg);
};

const queue = [];
const flush = () => {
  if (!mainSock || !queue.length) return;
  for (let msg of queue.splice(0)) mainSock.write(`${msg}\n`);
};
mod.write = (msg) => {
  queue.push(msg);
  while (queue.length > config.maxqueue) queue.shift();
  flush();
};

const destroySock = (sock) => {
  try {
    sock.destroy();
  } catch (sockDestroyError) {
    mod.emit("log", { sockDestroyError });
  }
};

const connect = async () => {
  const sock = new net.Socket();

  let dead;
  const die = (err) => {
    mod.emit("log", { err });

    if (dead) return;
    dead = true;

    if (mainSock === sock) mainSock = undefined;
    destroySock(sock);

    setTimeout(connect, config.chatsock_retry);
  };
  sock.on("close", () => die("closed"));
  sock.on("error", die);
  readline.createInterface({ input: sock }).on("line", recv(sock));

  sock.connect(config.sockpath);
  await new Promise((res) => sock.on("ready", res));
  mod.emit("log", { ready: true });

  if (mainSock) destroySock(mainSock);
  mainSock = sock;

  flush();
};
mod.connect = connect;
