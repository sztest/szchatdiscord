"use strict";

process.on("unhandledRejection", (err) => {
  throw err;
});

const mod_discord = require("./mod_discord");
const mod_chatsock = require("./mod_chatsock");

const mklog = (mod) => (msg) =>
  console.warn(
    JSON.stringify(
      Object.assign(
        {
          time: new Date().toISOString(),
          mod,
        },
        msg
      )
    )
  );
mod_discord.on("log", mklog("discord"));
mod_chatsock.on("log", mklog("chatsock"));

const sendto = (name, to) => {
  const log = mklog(name);
  return (msg) => {
    log({ msg });
    to.write(msg);
  };
};
mod_discord.on("msg", sendto("discord->game", mod_chatsock));
mod_chatsock.on("msg", sendto("game->discord", mod_discord));

mod_discord.connect();
mod_chatsock.connect();
