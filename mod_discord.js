"use strict";

const EventEmitter = require("events");
const discord = require("discord.js");
const emoji = require("node-emoji");

const config = require("./config");

const mod = new EventEmitter();
module.exports = mod;

const rxesc = (s) => s.replace(/[.*+?^${}()|[\]\\]/g, "\\$&");

let mainClient;

const recv = (client) => (msg) => {
  if (client !== mainClient)
    return mod.emit("log", {
      warn: "client !== mainClient",
      msg,
    });

  if (
    !msg ||
    !msg.channel ||
    msg.channel.deleted ||
    msg.channel.type !== "text" ||
    `${msg.channel.id}` !== `${config.channel}` ||
    !msg.author ||
    !msg.author.username ||
    !msg.content ||
    msg.author.id === client.user.id
  )
    return;

  let text = msg.content;

  const subst = {};
  if (msg.guild) {
    for (let r of msg.guild.roles.array())
      subst[rxesc(`<@&`) + "?" + rxesc(`${r.id}>`)] = `@${r.name}`;
    for (let c of msg.guild.channels.array())
      subst[rxesc(`<#${c.id}>`)] = `#${c.name}`;
  }
  for (let u of client.users.array())
    subst[rxesc(`<@!`) + "?" + rxesc(`${u.id}>`)] = `@${u.username}`;
  for (let [k, v] of Object.entries(subst))
    text = text.replace(new RegExp(k, "gi"), v);

  text = emoji.unemojify(text).replace(/<(:[^:]+:)\d+>/g, "$1");

  for (let line of text
    .split(/\r?\n/)
    .map((l) => `${l}`.trim())
    .filter((l) => l)) {
    const fmt = `<@${msg.author.username}> ${line}`.replace(
      /^(?:<[^\s>]+>\s*)+/,
      (m) => m.replace(/>\s*</g, "|")
    );
    mod.emit("msg", fmt);
  }
};

const queue = [];
const flush = () => {
  if (!mainClient || !queue.length) return;

  const chan = mainClient.channels.find(
    (c) => !c.deleted && c.type === "text" && `${c.id}` === `${config.channel}`
  );
  if (!chan) return;

  const subst = {};
  if (chan.guild) {
    for (let r of chan.guild.roles.array())
      if (r.mentionable) subst[rxesc(`@${r.name}`)] = `<@&${r.id}>`;
    for (let c of chan.guild.channels.array())
      subst[rxesc(`#${c.name}`)] = `<#${c.id}>`;
  }
  for (let u of mainClient.users.array())
    subst[rxesc(`@${u.username}`)] = `<@!${u.id}>`;
  for (let u of chan.members.array().map((u) => u.user || u))
    subst[rxesc(`@${u.username}`)] = `<@!${u.id}>`;

  for (let msg of queue.splice(0)) {
    for (let [k, v] of Object.entries(subst))
      msg = msg.replace(new RegExp(k, "gi"), v);
    chan.send(msg);
  }
};
mod.write = (msg) => {
  queue.push(msg);
  while (queue.length > config.maxqueue) queue.shift();
  flush();
};

const destroyClient = (client) => {
  try {
    client.destroy();
  } catch (clientDestroyError) {
    mod.emit("log", { clientDestroyError });
  }
};

let delay = config.discord_delaymin;
const connect = async () => {
  const client = new discord.Client();

  client.on("warn", (warn) => mod.emit("log", { warn }));
  client.on("error", (err) => {
    mod.emit("log", { err, delay });

    if (mainClient === client) mainClient = undefined;
    destroyClient(client);

    setTimeout(connect, delay);
    delay = delay * config.discord_backoff;
    if (delay > config.discord_delaymax) delay = config.discord_delaymax;
  });
  client.on("message", recv(client));

  client.login(config.token);
  await new Promise((res) => client.on("ready", res));
  mod.emit("log", { ready: true });
  delay = config.discord_delaymin;

  if (mainClient) destroyClient(mainClient);
  mainClient = client;

  if (config.activity) client.user.setActivity(config.activity);

  flush();
};
mod.connect = connect;
