# Discord <=> szutil_chatsocket Chat Bridge

This is bidirectional chat relay between one Minetest server (using the szutil_chatsocket mod) and one Discord channel (using a discord bot token).

## Key Features

- Maintain connections independently for MT and Discord (instead of crashing when either side goes down and forcing a reconnect of the other)
- Messages received while relay is down are queued and sent when reconnected (up to a small safety limit)
- Translation of Discord's user/role/channel mention formats
- Convert emoji to texual names
- Exponential backoff for Discord reconnection (required to prevent blocking due to abuse)
- Coalescing nested relays from `<x> <y> <z> w` to `<x|y|z> w`

## Setup

### Discord Side

- First, log in to https://discord.com/developers and create a new application.
- Create a bot for the application, and _save the token_.
- Setup anything else you want (name, image).
- Make sure to enable the "privileged intents"; not sure which of these is really needed, but it wasn't tested without.
- Add the bot to your Discord guild with `https://discord.com/oauth2/authorize?client_id=`(general info -> application id)`&permissions=199680&scope=bot`
- Setup the necessary channel and Discord-side privs for the bot.
- Make sure Discord Developer Mode is enabled, then copy the ID for the channel to be bridged.

### Minetest Side

- Install [szutilpack](https://gitlab.com/sztest/szutilpack) (or update an existing install to latest version).
- Make sure either `szutil_chatsocket` is in `secure.trusted_mods`, or mod security is disabled.
- Enable the `szutil_chatsocket` mod.
- Start the server.

### Launch relay

In this project, launch:

`node . --sockpath=`(path/to/chat.sock)` --token='`(discord-bot-token)`' --channel=`(discord-channel-id)

You can also add an optional `--activity=`(activity-text) to set the bot's presence activity at startup. The bot's online presence is automatically determined based on the bridge staying connected.
